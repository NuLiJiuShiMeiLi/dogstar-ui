# dogstar

天狼星 基于 VUE 框架的模拟Windows 桌面效果，后台管理UI。
[预览](https://ispace-code.gitee.io/dogstar-ui)

欢迎大家加入共同建设。
![设置壁纸](https://images.gitee.com/uploads/images/2020/0716/143216_7117f6e0_15710.png "11.png")
![加入模态对话框和抽屉](https://images.gitee.com/uploads/images/2020/0727/092359_d8174fd6_15710.png "捕获.PNG")
![效果图](https://images.gitee.com/uploads/images/2020/0709/201734_b9a680d4_15710.png "2.png")

加入了模态对话框和抽屉效果。在窗口和桌面均可使用。

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
